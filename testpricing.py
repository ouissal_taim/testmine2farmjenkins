#test priciping
import unittest
import pandas as pd
import time
import xmlrunner

import unittest
import pandas as pd
import time

from app.config import env
from app.config.env import ScenarioGeneratorType, PipelineLayer
from app.config.env_func import reset_db_name
from app.data.DataManager import DataManager
from app.model.Simulator import Simulator
from app.tools import Utils
from app.model.ScenarioGenerator import ScenarioGeneratorFactory as SGF


reset_db_name("mine2farm")
dm = DataManager()
dm.load_data()





def runner(output='python_tests_xml'):
    return xmlrunner.XMLTestRunner(
        output=output
    )


class PricingTestSuite(unittest.TestCase):
    """Pricing test cases."""
    def __init__(self, *args, **kwargs):
        super(PricingTestSuite, self).__init__(*args, **kwargs) #super class 
        scenarios_df = pd.read_csv(env.APP_FOLDER + "tests/data/one_scenario.csv") #CSV Folders 
        self.scenarios_dic = Utils.get_scenario_from_df(scenarios_df)  #Scenario
        scenario_id = 1
        self.simulator = Simulator(dm=dm, monikers_filter=sum(self.scenarios_dic[scenario_id], [])) #simulation is a representation of a real-world system
        scenarios = [
            self.simulator.nodes[layer] for layer in [
                PipelineLayer.PAP, PipelineLayer.SAP,
                PipelineLayer.BENEFICIATION, PipelineLayer.MINE, PipelineLayer.MINE_BENEFICIATION
            ] if layer in self.simulator.nodes
        ]
        self.scenario_generator = SGF.create_scenario_generator(ScenarioGeneratorType.SPECIFIC_SCENARIOS,
                                                                self.simulator, [scenarios])

    def test_pricing(self):
        result, _ = self.simulator.simulate(scenario_generator=self.scenario_generator)
        self.assertTrue(result[1]["Cost PV"] == 12057687291.92442)


    def test_twice(self):
        self.test_pricing()
        self.test_pricing()


if __name__ == '__main__':
    runner().run(unittest())
